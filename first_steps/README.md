# 4ME Installation tutorial

## Introduction
4ME installation is hard. It involves a lot of moving parts and this could be overwhelming for a new user.

This tutorial will cover the first steps involved to get a running 4ME installation.

At the end of this tutorial, you'll have a working barebone 4ME install composed of a working UI with a working control room map service.

## Prerequisites
This tutorial assumes the following :
* A linux workstation with this software installed :
  * `git`
  * `curl`
  * `docker (v1.11 or higher)`
  * `docker-compose (v1.9.0 or higher)`
  * A webbrowser (`chromium` or `chrome` is preferred, even though the app should work with firefox with a few layout quirks)
* `docker` running
* An account on this linux workstation with `docker privileges`
* Basic shell knowledge (typing commands, navigating between directories)
* Basic git knowledge (clone a repo, navigate between branches)
* A relatively fast internet connection to download `docker` images

## How to get started and use this tutorial
This tutorial will take you through a series of steps. Each step is described in a subdirectory of this repository. In each step, you'll find a `README.md` file, describing the purpose of this step. Once you're done with a step, just `cd` into the next step.

To get started, simply clone this repository on your linux workstation and navigate to [step-01][step-01] directory.

## List of steps
* [Step 01: First steps with Docker][step-01]
* [Step 02: Run your first container][step-02]
* [Step 03: Opening a container to the outside world][step-03]
* [Step 04: Injecting stuff in a container][step-04]
* [Step 05: Configuring `4me.frontend`][step-05]
* [Step 06: Configuring `4me.core.mapping`][step-06]
* [Step 07: docker-compose][step-07]

## If you have no knowledge of Docker, go to [step 01][step-01]
## If you know your way around docker, go directly to [step 05][step-05]

## Sidenote to future readers
This tutorial was written on 2016-12-06. This tutorial will use 4me software released around this date which could become outdated at some point in the future. This is not a concern since our release process won't likely change in the future.  
If we were to introduce breaking changes in our release process that could impact the tutorial, we'll try our best to update this repo.

[step-01]: ./step-01
[step-02]: ./step-02
[step-03]: ./step-03
[step-04]: ./step-04
[step-05]: ./step-05
[step-06]: ./step-06
[step-07]: ./step-07
