# Step 05 - Configuring `4me.frontend`

> Objectives:
> * Get 4me.frontend running with proper configuration

## Prerequisites
This step assumes you have completed [step 04][step-04] and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

You should also have no container running.

## Remember, at the end of [step 03][step-03] ...
... our app was complaining about a missing `config.api.js` file.

[Good thing I brought one with me !][config]

Here's the content of the file so we can discuss it:
```JavaScript
window.FOURME_CONFIG = {
  FOURME_ENV: 'LFEE',
  overrideCwpId: 32,
  disabledOrgans: ['xman', 'etfmsProfile'],
  core_mapping_url: 'http://localhost:3200',
  mapping_url: 'http://localhost:3200',
};
```

Don't be scared, it's just JavaScript. It contains a bunch of data that our frontend app needs to work properly. We'll dive deeper into it later in this chapter but for now, let's just run a container with this data injected it.

## We have the missing file, we just don't know where to put it

In [step 03][step-03], this was the output that was served to our browser :
```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>4ME</title>
</head>
<body>
<script type="text/javascript" src="/js/config.api.js"></script>

<div id="app"></div>

<script type="text/javascript" src="/js/app.js"></script><script type="text/javascript" src="/js/vendor.js"></script></body>
</html>
```

See this line ?
```html
<script type="text/javascript" src="/js/config.api.js"></script>
```

It tells our browser to load the file on the path `/js/config.api.js` which is relative to the webroot. We need to inject the file so our `nginx` trapped in our container can serve the file on the path the browser expect it.

The file should be placed here `/usr/share/nginx/html/js/config.api.js` in the container. That's how `4me.frontend` image works. Trust me, I built it.

## Let's run the container
Ok, download the [config][config] file and place it somewhere on your workstation. For the rest of the tutorial, I'll assume it's placed in a `/data/4me.frontend` directory.

So technically, the full path to the `config.api.js` file is now `/data/4me.frontend/config.api.js` on your workstation.

Let's run our container, with a defined name of `4me_frontend`, with the `config.api.js` file injected in the right place and with the internal port 80 published on the host port 80.
```
# docker run -d -v /data/4me.frontend/config.api.js:/usr/share/nginx/html/js/config.api.js:ro -p 80:80 --name 4me_frontend registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
61b4b3687923d07573fc7c10c870953e4a42d0a25262f82e5567945c5a68223a
```

> Notice how the docker run command is getting longer and longer ? Docker has a tool to make it easier, we'll talk about it later !

And point your browser to `http://localhost`.

Here's what you should see :
![step05][step05_image]

PROGRESS !

At this point, our frontend container is properly configured. **Keep it running, we'll need it later.**

## `4me.frontend` config file explained
Let's go back to our [config file][config] and its content.
```JavaScript
window.FOURME_CONFIG = {
  FOURME_ENV: 'LFEE',
  overrideCwpId: 32,
  disabledModules: ['xman', 'etfmsProfile'],
  core_mapping_url: 'http://localhost:3200',
  mapping_url: 'http://localhost:3200',
};
```
The first and last lines :
```JavaScript
window.FOURME_CONFIG = {
  ...
};
```
They attach a global JS object to the `window` object called `FOURME_CONFIG`. This `FOURME_CONFIG` object is later accessed by our bundled app to configure itself.

```JavaScript
  FOURME_ENV: 'LFEE',
```
This configure the app to use the `LFEE` environment. Our app uses a custom environment library called `4me.env`, which (will eventually) provides static environment data to the whole 4ME ecosystem : sectors, 4me clients, sector groups, frequencies ... The source code can be found [here][4me_env] and you're welcome to contribute if you wish to setup a different environment (LFRR or LFBB for instance !).

```JavaScript
  overrideCwpId: 32,
```
This one is a bit tricky. Our app has to know which 4ME client it's running for. For instance, when the app runs on a CWP, a zoom level will be applied to the whole UI to make it more readable for the ATCO. In production, our frontend app relies on the `4me.core.mapping` service to provide this information on startup (when the app starts in your browser). We could store a cookie or a configuration file on each client, but our goal was to keep the clients as similar as possible.  
For development purpose, you can override this identification process and provide your own cwpId. The id references what's defined in the env library ([`4me.env`][4me_env]).

```JavaScript
  disabledModules: ['xman', 'etfmsProfile'],
```
4ME frontend app is composed of multiple subapps. This lets you disable specific sub applications. In our case, since we're only building a minimal 4ME stack, we disable XMAN and ETFMS PROFILE.

```JavaScript
  core_mapping_url: 'http://localhost:3200',
  mapping_url: 'http://localhost:3200',
```
4ME frontend app needs to know where to look for the `4me.core.mapping` service. The duplication is intentional to prepare a future split in the backend code. `core_mapping_url` tells the `core` part of the frontend app where to look for its backend. Likewise, `mapping_url` tells the `mapping` sub app (the one displaying the control room).  
Here, we tell our frontend app that the `4me.core.mapping` is expected to run on `localhost:3200`.

## Moving on

Ok, we now have a functional `4me.frontend` container, but it's useless without it's backend service. [Step 06][step-06] will focus on the `4me.core.mapping` backend service.

[config]: ./config.api.js
[config_example]: https://gitlab.com/devteamreims/4me.frontend/blob/master/src/config.api.js.example
[step05_image]: ../images/step05.png
[4me_env]: https://gitlab.com/devteamreims/4me.env
[step-06]: ../step-06
[step-03]: ../step-03
[step-04]: ../step-04
